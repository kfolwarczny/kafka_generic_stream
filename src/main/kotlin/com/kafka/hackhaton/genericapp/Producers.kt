/*
package com.kafka.hackhaton.genericapp

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.kafka.hackhaton.genericapp.model.Message
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Service
import java.time.Instant


@SpringBootApplication
class ProducerApplication : CommandLineRunner {

    private val objectMapper = jacksonObjectMapper().registerModules(JavaTimeModule())

    @Autowired
    private val producer: Producer? = null

    @Throws(Exception::class)
    override fun run(vararg args: String) {
        val sample = Message("uuid", "to", "from", Instant.parse("2019-09-19T22:00:00Z"))

        producer!!.sendMessage(TOPIC, "uuid", objectMapper.writeValueAsString(sample))
    }

    companion object {
        private val TOPIC = "bka_first"

        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(ProducerApplication::class.java, *args)
        }
    }
}

@Service
class Producer {

    @Autowired
    private val kafkaTemplate: KafkaTemplate<String, String>? = null

    fun sendMessage(topic: String, key: String, message: String) {
        logger.info(String.format("Producing message: %s,  %s", key, message))
        this.kafkaTemplate!!.send(topic, key, message)
    }

    companion object {
        private val logger = LoggerFactory.getLogger(Producer::class.java)
    }
}

*/
