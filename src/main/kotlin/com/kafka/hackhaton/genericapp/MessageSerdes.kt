package com.kafka.hackhaton.genericapp

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.kafka.hackhaton.genericapp.model.Message
import org.apache.kafka.common.serialization.Deserializer
import org.apache.kafka.common.serialization.Serializer
import org.slf4j.LoggerFactory
import java.io.IOException
import java.nio.charset.Charset

class MessageSerializer(private var objectMapper: ObjectMapper?) : Serializer<Message> {

    override fun configure(configs: Map<String, *>, isKey: Boolean) {
        // Not used
    }

    override fun serialize(topic: String, data: Message?): ByteArray? {
        return try {
            if (data == null) null else this.objectMapper!!.writeValueAsBytes(data)
        } catch (e: JsonProcessingException) {
            LOGGER.warn("Cannot serialize {}", data, e)
            ByteArray(0)
        }
    }

    override fun close() {
        this.objectMapper = null
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(MessageSerializer::class.java)
    }
}

class MessageDeserializer(private var objectMapper: ObjectMapper?) : Deserializer<Message> {

    override fun configure(configs: Map<String, *>, isKey: Boolean) {
        // Not used
    }

    override fun deserialize(topic: String, data: ByteArray?): Message? {
        if (data == null) {
            return null
        }

        return try {
            objectMapper!!.readValue<Message>(data)
        } catch (e: IOException) {
            LOGGER.warn("Cannot deserialize {}", String(data, Charset.defaultCharset()), e)
            null
        }
    }

    override fun close() {
        this.objectMapper = null
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(MessageDeserializer::class.java)
    }
}
