package com.kafka.hackhaton.genericapp

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "application.topics")
class TopicsProperties(
    var input: String = "bca_input_topic",
    var output: String = "bca_output_topic"
)

