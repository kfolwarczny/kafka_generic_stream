package com.kafka.hackhaton.genericapp.model

import com.fasterxml.jackson.annotation.JsonFormat
import java.beans.ConstructorProperties
import java.time.Instant


data class Message
@ConstructorProperties("uuid", "to", "from", "timestamp")
constructor(
         val uuid: String,
         val to: String,
         val from: String,
         @JsonFormat(shape = JsonFormat.Shape.STRING)
         val timestamp: Instant
) {
    companion object {
        val EMPTY = Message("not uuid", "error_to", "error_from", Instant.now())
    }
}