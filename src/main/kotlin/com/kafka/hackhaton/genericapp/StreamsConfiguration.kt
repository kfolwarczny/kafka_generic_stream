package com.kafka.hackhaton.genericapp

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.time.Clock

@Configuration
class StreamsConfiguration {

    @Bean
    fun objectMapper(): ObjectMapper {
        return jacksonObjectMapper().registerModules(JavaTimeModule())
    }

    @Bean
    fun clock(): Clock {
        return Clock.systemUTC()
    }
}
