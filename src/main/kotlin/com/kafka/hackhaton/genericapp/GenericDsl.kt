package com.kafka.hackhaton.genericapp

import arrow.core.Try
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.kafka.hackhaton.genericapp.model.Message
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.kstream.Consumed
import org.apache.kafka.streams.kstream.KStream
import org.apache.kafka.streams.kstream.KeyValueMapper
import org.apache.kafka.streams.kstream.Produced
import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.stereotype.Service
import java.time.Clock
import javax.annotation.PostConstruct


@EnableConfigurationProperties(AppProperties::class)
@Service
class GenericDsl(private val streamsBuilder: StreamsBuilder,
                 private val objectMapper: ObjectMapper,
                 private val topics: TopicsProperties,
                 private val appProperties: AppProperties,
                 private val clock: Clock) {

    @PostConstruct
    fun build() {
        readFromInputTopic()
                .map(mapper)
                .persist()
    }


    val mapper = KeyValueMapper<String, Message, KeyValue<String, Message>> { key, message ->
        LOGGER.info("Mapping message $message with key $key")
        KeyValue(key, message.copy(
                timestamp = clock.instant(),
                from = appProperties.name,
                to = topics.output
        ))
    }

    private fun readFromInputTopic(): KStream<String, Message> {
        return streamsBuilder.stream(
                topics.input,
                Consumed.with(
                        Serdes.String(), Serdes.String()
                )
        )
                .map { key: String?, value ->
                    LOGGER.info("Trying to map $value to message")
                    KeyValue(key ?: "no_key",
                            Try { objectMapper.readValue<Message>(value) }
                                    .fold({
                                        LOGGER.error("Could not read message $value", it)
                                        Message.EMPTY
                                    }, { it }))
                }
    }

    private fun KStream<String, Message>.persist() {
        this.to(
                topics.output,
                Produced.with(
                        Serdes.String(),
                        Serdes.serdeFrom(MessageSerializer(objectMapper), MessageDeserializer(objectMapper))
                )
        )
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(GenericDsl::class.java)
    }
}
