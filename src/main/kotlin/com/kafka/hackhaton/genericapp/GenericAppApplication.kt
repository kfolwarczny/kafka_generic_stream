package com.kafka.hackhaton.genericapp

import org.springframework.boot.autoconfigure.ImportAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration
import org.springframework.boot.autoconfigure.web.reactive.ReactiveWebServerFactoryAutoConfiguration
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.kafka.annotation.EnableKafkaStreams

@SpringBootApplication
@EnableKafkaStreams
@ImportAutoConfiguration(
		ReactiveWebServerFactoryAutoConfiguration::class,
		KafkaAutoConfiguration::class
)
@EnableConfigurationProperties(value = [TopicsProperties::class])
class GenericAppApplication

fun main(args: Array<String>) {
	runApplication<GenericAppApplication>(*args)
}
