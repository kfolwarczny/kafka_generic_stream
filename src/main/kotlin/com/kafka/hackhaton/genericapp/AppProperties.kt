package com.kafka.hackhaton.genericapp

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "application")
class AppProperties(
        var name: String = "generic-app-name"
)

