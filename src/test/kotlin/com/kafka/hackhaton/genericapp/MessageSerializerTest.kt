package com.kafka.hackhaton.genericapp

import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Test

class MessageSerializerTest {

    private val messageSerializer = MessageSerializer(StreamsConfiguration().objectMapper())

    @Test
    fun `should serialize`() {
        val serialized = messageSerializer.serialize("something", sample())

        assertNotNull(serialized)
    }

    @Test
    fun `returns null for null message`() {
        val serialized = messageSerializer.serialize("something", null)

        assertNull(serialized)
    }
}

class MessageDeserializerTest {

    private val deserializer = MessageDeserializer(StreamsConfiguration().objectMapper())

    @Test
    fun `deserilizes null`() {

        val result = deserializer.deserialize("something", null)

        assertNull(result)
    }
}