package com.kafka.hackhaton.genericapp

import com.kafka.hackhaton.genericapp.model.Message
import java.time.Instant


fun sample(): Message {
    return Message(java.util.UUID.randomUUID().toString(), "to", "from", Instant.parse("2019-09-19T22:00:00Z"))
}