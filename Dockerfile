FROM openjdk:11-jdk-stretch

ENV SERVICE_HOME=/opt/generic-app
ENV APP_NAME=bca_generic_app
ENV INPUT_TOPIC=bca_input_topic
ENV OUTPUT_TOPIC=bca_output_topic

RUN mkdir -p ${SERVICE_HOME}

ADD ./build/libs/generic-app-0.0.1-SNAPSHOT.jar ${SERVICE_HOME}/generic-app.jar

CMD java -jar ${SERVICE_HOME}/generic-app.jar
